#ifndef MY_BUTTON_H
#define MY_BUTTON_H

#include <Arduino.h>
#include "Digit.h"
class Button {
  
  private:
    String label;
    byte pin;
    bool pressed;
    void init();
    
  public:
    Button(String label, byte pin);
    bool ReadPin();
    bool IsJustPressed();
    static bool ProcessInput();
};
extern Button buttons[];

#endif
