//www.elegoo.com
//2016.12.12
#include "Button.h"
#include "Digit.h"

// pin assignment
int digit1 = 7;   //pin to show digit (LOW shows)
int digit2 = 6;   //pin to show digit (LOW shows)
int digit3 = 5;   //pin to show digit (LOW shows)
int digit4 = 4;   //pin to show digit (LOW shows)
int answerLed1 = 11;  //pin for answer led digit (HIGH lights)
int answerLed2 = 12;  //pin for answer led digit (HIGH lights)
int answerLed3 = 2;   //pin for answer led digit (HIGH lights)
int answerLed4 = 3;   //pin for answer led digit (HIGH lights)
byte button_left = A4; //pin for button left (used digitally)
byte button_right = A3; //pin for button right (used digitally)
byte button_up = A2; //pin for button up (used digitally)
byte button_down = A1; //pin for button down (used digitally)
byte button_select = A0; //pin for button select (used digitally)

int roundsLeft;

// making objects and putting them in arrays
// Load buttons, uses
Button buttons[] = {
  Button("LEFT", button_left),
  Button("RIGHT", button_right),
  Button("UP", button_up),
  Button("DOWN", button_down),
  Button("SELECT", button_select)
};

Digit digits[] = {
  Digit(0, 0, digit1, answerLed1),
  Digit(1, 0, digit2, answerLed2),
  Digit(2, 0, digit3, answerLed3),
  Digit(3, 0, digit4, answerLed4)
};

void setup() {
  // Set serial output for debugging on 9600 Baud
  Serial.begin(9600);

  roundsLeft = 10;

  // Set pins, pinModes for , leds and button inputs (A0 - A4) are set in the constructor of their class
  pinMode(Digit::latch, OUTPUT);
  pinMode(Digit::clock, OUTPUT);
  pinMode(Digit::data, OUTPUT);

  long timeElapsed = millis();
  while (!buttons[4].IsJustPressed())
  {
    timeElapsed = millis();
  }
  randomSeed(timeElapsed);
  Digit::InitSolution();
}

void loop() {
  if (Button::ProcessInput())
  {
    if ( Digit::SubmitAnswer() )  // returns true if won
    {
      Digit::ShowFinishLoop(true);  // argument = won yes/no
      roundsLeft = 10;
    } else {
      roundsLeft--;
      if (roundsLeft == 0) {
        Digit::ShowFinishLoop(false);  // argument = won yes/no
        roundsLeft = 10;
      }
    }
  }
  Digit::Update();  // Handle display logic
}
