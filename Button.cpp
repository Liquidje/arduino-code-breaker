#include "Button.h"
Button::Button(String label, byte pin) {
  this->label = label;
  this->pin = pin;
  this->pressed = false;
  init();
}
void Button::init() {
  pinMode(pin, INPUT_PULLUP);
}

// Prevents duplicate input
bool Button::IsJustPressed() {
  bool value;
  if(this->ReadPin())
  {
    value = this->pressed ? false : true; // Return false if the button is already pressed
    this->pressed = true;
  }
  else
  {
    this->pressed = false;
    value = false;
  }
  return value;
}

bool Button::ReadPin() {
  bool value = digitalRead(this->pin) == LOW ? true : false;
  return value;
}

bool Button::ProcessInput() {
  // Handle digit selection
  Digit* selectedDigit = Digit::GetSelectedDigit();
  if( buttons[0].IsJustPressed()){ selectedDigit->SetSelectedDigitToPrevious();}  // Left button
  if( buttons[1].IsJustPressed()){ selectedDigit->SetSelectedDigitToNext();}      // Right button
  
  // Handle digit value
  if(buttons[2].IsJustPressed()){selectedDigit->IncreaseValue();} // Up button
  if(buttons[3].IsJustPressed()){selectedDigit->DecreaseValue();} // Down button
  
  // Handle select button
  if(buttons[4].IsJustPressed()){ 
    return true; 
  }
  return false;
}
